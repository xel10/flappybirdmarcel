﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kirby : MonoBehaviour
{
    public float speed;
    private bool fly;
    private bool isDead;
    private Animator anim;
    private GameManager gameManager;
	public AudioSource gameover;
	public AudioSource game;
    public void Initialize ()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        anim = GetComponentInChildren<Animator>();
        fly = false;
	}
	
	public void MyUpdate ()
    {
		
		if(fly) transform.Translate(Vector2.up * speed * Time.deltaTime);
        else transform.Translate(Vector2.down * speed * Time.deltaTime);
    }

    public void DeadUpdate()
    {
		
		transform.Translate(Vector2.down * speed * Time.deltaTime);
    }

    public void Fly()
    {
        if(isDead) return;

        fly = true;
        anim.Play("Fly");

    }

    public void Fall()
    {
        if(isDead) return;
	
        fly = false;
        anim.Play("Fall");
    }

    void Dead()
    {
        if(isDead) return;
        isDead = true;
        anim.Play("Dead");

        gameManager.GameOver();
		game.Stop();
		gameover.Play();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            Dead();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Boundary")
        {
            Dead();
        }
    }
}
